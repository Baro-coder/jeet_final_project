/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap;

import java.util.List;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Script;

/**
 *
 * @author student
 */
@WebService(serviceName = "SOAP")
@Stateless()
public class SOAP {

    private EntityManager em;
    
    @WebMethod(operationName = "getAllScripts")
    public List<Script> getAllScripts(){
        checkManager();
        List<Script> list = em.createNamedQuery("Script.findAll", Script.class).getResultList();
        return list;
    }
    
    /**
     *
     * @param id
     * @return
     */
    @WebMethod(operationName = "getOneScript")
    public Script getOneScript(@WebParam(name="id") Integer id){
        checkManager();
        Script scr = em.createNamedQuery("Script.findById", Script.class).setParameter("id", id).getSingleResult();    
        Logger.getLogger(SOAP.class.getName()).info(scr.getDescription());
        return scr;
    }
    
    @WebMethod(operationName = "uploadScript")
    public void uploadScript(@WebParam(name="name")String name, @WebParam(name="description")String description, @WebParam(name="script")String script){
        checkManager();
        Script scr = new Script(name, description, script);
        em.persist(scr);
    }
    
    @WebMethod(operationName = "deleteScript")
    public void deleteScript(@WebParam(name="id")Integer id){
        checkManager();
        em.createNamedQuery("Script.deleteById", Script.class).setParameter("id", id).executeUpdate();
        
    }

    private void checkManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY19IT1S0_SZYKULA_PROJEKT-webservicePU");
        em = emf.createEntityManager();
    }
}
