<%-- 
    Document   : confirmDelete
    Created on : 2022-06-29, 21:00:06
    Author     : student
--%>

<%@page import="scriptservice.Script"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirm delete</title>
    </head>
    <body>
        <jsp:useBean id="controller" class="ejb.ControllerBean" scope="request"/>
        <%
            Script s = controller.getScript(Integer.valueOf(request.getParameter("id")));
            request.setAttribute("script", s);
        %>
        <h1>Are you sure?</h1>
        <h2 style="color: red;">Do you really want to delete "${script.name}"?</h2>
        <a href="deleted.jsp?id=${script.id}"><button>Delete</button></a>
        <a href="home.jsp"><button type="button">Back Home</button></a>
    </body>
</html>
