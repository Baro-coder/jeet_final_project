<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New script</title>
    </head>
    <body>
        <h1>New script</h1>
        <form action="created.jsp" method="POST" enctype="multipart/form-data" style="width: 500px; margin-left: 50px;">
            Description<br/>
            <textarea rows="4" name="description" style="width: 100%;"></textarea><br/><br/>
            Script file<br/>
            <input type="file" name="file" style="width: 100%;"/><br/><br/>
            <button type="submit" style="width: 200px;">Create</button>
            <a href="home.jsp"><button type="button" style="width: 200px;">Cancel</button></a>
        </form>
    </body>
</html>
