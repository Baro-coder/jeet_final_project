
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="scriptservice.Script"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View script</title>
    </head>
    <body>
        <jsp:useBean id="controller" class="ejb.ControllerBean" scope="request"/>
        <%
            Script s = controller.getScript(Integer.valueOf(request.getParameter("id")));
            request.setAttribute("script", s);
        %>
        <h1>${requestScope.script.name}</h1>
        <h2>Description: ${requestScope.script.description}</h2>
        
        <a href="download.jsp?id=${script.id}"><button>Download</button></a>
        <a href="home.jsp"><button type="button">Back Home</button></a>
    </body>
</html>
