<%@page import="scriptservice.Script"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:useBean id="controller" class="ejb.ControllerBean" scope="request"/>
<%
    Integer id = Integer.valueOf(request.getParameter("id"));
    Script s = controller.getScript(id);
    
    response.setContentType("text/plain");
    response.setHeader("Content-Disposition", "attachment; filename=\""+s.getName()+"\"");
    response.getOutputStream().write(s.getScript().getBytes());
    response.getOutputStream().flush();
    response.getOutputStream().close();
    response.flushBuffer();
   
%>

