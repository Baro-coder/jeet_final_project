<%-- 
    Document   : home
    Created on : 2022-06-29, 20:59:38
    Author     : student
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Existing scripts</title>
        <style>
            table{
                border-collapse: collapse;
                width: 800px;
            }
            table td, table th {
                border: 1px solid green;
            }
        </style>
    </head>
    <body>
        <jsp:useBean id="controller" class="ejb.ControllerBean" scope="request"/>
        <h1>Home page</h1>
        <a href="create.jsp"><button>New script</button></a>
        <c:set var="scripts" value="${controller.getAllScripts()}"/>
        <c:if test="${scripts.size() > 0}">
            <h2>Existing scripts</h2>
            <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>View</th>
                <th>Delete</th>
            </tr>
            <c:forEach var="scr" items="${scripts}">
                <tr>
                    <td>${scr.id}</td>
                    <td>${scr.name}</td>
                    <td style="text-align: center"><a href="view.jsp?id=${scr.id}"><button>View</button></a></td>
                    <td style="text-align: center"><a href="delete.jsp?id=${scr.id}"><button>Delete</button></a></td>
                </tr>
            </c:forEach>
        </table>
        </c:if>
    </body>
</html>
