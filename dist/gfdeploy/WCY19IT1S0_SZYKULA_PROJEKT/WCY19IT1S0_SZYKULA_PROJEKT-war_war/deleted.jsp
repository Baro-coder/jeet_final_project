<%-- 
    Document   : uploaded
    Created on : 2022-06-29, 21:00:13
    Author     : student
--%>

<%@page import="scriptservice.Script"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Script deleted</title>
    </head>
    <body>
        <jsp:useBean id="controller" class="ejb.ControllerBean" scope="request"/>
        <% 
            try{
                Integer id = Integer.valueOf(request.getParameter("id"));

                //send
                controller.deleteScript(id);

                //prepare result to show
                pageContext.setAttribute("success", true);
            } catch(Exception e){
                pageContext.setAttribute("success", false);
                pageContext.setAttribute("error", e.getMessage());
            }
        %>
        
            <c:if test="${success == true}">
                <h1>Script deleted!</h1>    
            </c:if>
            <c:if test="${success == false}">
                <h1 style="color: red;">An error occured!</h1>
                <h2 style="color: red;">Error message: ${error}</h2>
            </c:if>
        
            <a href="home.jsp"><button type="button">Back Home</button></a>
    </body>
</html>
