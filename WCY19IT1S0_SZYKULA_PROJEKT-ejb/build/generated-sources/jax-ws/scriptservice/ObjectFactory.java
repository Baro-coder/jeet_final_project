
package scriptservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the scriptservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteScript_QNAME = new QName("http://soap/", "deleteScript");
    private final static QName _DeleteScriptResponse_QNAME = new QName("http://soap/", "deleteScriptResponse");
    private final static QName _GetAllScripts_QNAME = new QName("http://soap/", "getAllScripts");
    private final static QName _GetAllScriptsResponse_QNAME = new QName("http://soap/", "getAllScriptsResponse");
    private final static QName _GetOneScript_QNAME = new QName("http://soap/", "getOneScript");
    private final static QName _GetOneScriptResponse_QNAME = new QName("http://soap/", "getOneScriptResponse");
    private final static QName _Script_QNAME = new QName("http://soap/", "script");
    private final static QName _UploadScript_QNAME = new QName("http://soap/", "uploadScript");
    private final static QName _UploadScriptResponse_QNAME = new QName("http://soap/", "uploadScriptResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: scriptservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteScript }
     * 
     */
    public DeleteScript createDeleteScript() {
        return new DeleteScript();
    }

    /**
     * Create an instance of {@link DeleteScriptResponse }
     * 
     */
    public DeleteScriptResponse createDeleteScriptResponse() {
        return new DeleteScriptResponse();
    }

    /**
     * Create an instance of {@link GetAllScripts }
     * 
     */
    public GetAllScripts createGetAllScripts() {
        return new GetAllScripts();
    }

    /**
     * Create an instance of {@link GetAllScriptsResponse }
     * 
     */
    public GetAllScriptsResponse createGetAllScriptsResponse() {
        return new GetAllScriptsResponse();
    }

    /**
     * Create an instance of {@link GetOneScript }
     * 
     */
    public GetOneScript createGetOneScript() {
        return new GetOneScript();
    }

    /**
     * Create an instance of {@link GetOneScriptResponse }
     * 
     */
    public GetOneScriptResponse createGetOneScriptResponse() {
        return new GetOneScriptResponse();
    }

    /**
     * Create an instance of {@link Script }
     * 
     */
    public Script createScript() {
        return new Script();
    }

    /**
     * Create an instance of {@link UploadScript }
     * 
     */
    public UploadScript createUploadScript() {
        return new UploadScript();
    }

    /**
     * Create an instance of {@link UploadScriptResponse }
     * 
     */
    public UploadScriptResponse createUploadScriptResponse() {
        return new UploadScriptResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteScript }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "deleteScript")
    public JAXBElement<DeleteScript> createDeleteScript(DeleteScript value) {
        return new JAXBElement<DeleteScript>(_DeleteScript_QNAME, DeleteScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteScriptResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteScriptResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "deleteScriptResponse")
    public JAXBElement<DeleteScriptResponse> createDeleteScriptResponse(DeleteScriptResponse value) {
        return new JAXBElement<DeleteScriptResponse>(_DeleteScriptResponse_QNAME, DeleteScriptResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllScripts }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllScripts }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "getAllScripts")
    public JAXBElement<GetAllScripts> createGetAllScripts(GetAllScripts value) {
        return new JAXBElement<GetAllScripts>(_GetAllScripts_QNAME, GetAllScripts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllScriptsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllScriptsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "getAllScriptsResponse")
    public JAXBElement<GetAllScriptsResponse> createGetAllScriptsResponse(GetAllScriptsResponse value) {
        return new JAXBElement<GetAllScriptsResponse>(_GetAllScriptsResponse_QNAME, GetAllScriptsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOneScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOneScript }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "getOneScript")
    public JAXBElement<GetOneScript> createGetOneScript(GetOneScript value) {
        return new JAXBElement<GetOneScript>(_GetOneScript_QNAME, GetOneScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOneScriptResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOneScriptResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "getOneScriptResponse")
    public JAXBElement<GetOneScriptResponse> createGetOneScriptResponse(GetOneScriptResponse value) {
        return new JAXBElement<GetOneScriptResponse>(_GetOneScriptResponse_QNAME, GetOneScriptResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Script }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Script }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "script")
    public JAXBElement<Script> createScript(Script value) {
        return new JAXBElement<Script>(_Script_QNAME, Script.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UploadScript }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "uploadScript")
    public JAXBElement<UploadScript> createUploadScript(UploadScript value) {
        return new JAXBElement<UploadScript>(_UploadScript_QNAME, UploadScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadScriptResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UploadScriptResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap/", name = "uploadScriptResponse")
    public JAXBElement<UploadScriptResponse> createUploadScriptResponse(UploadScriptResponse value) {
        return new JAXBElement<UploadScriptResponse>(_UploadScriptResponse_QNAME, UploadScriptResponse.class, null, value);
    }

}
