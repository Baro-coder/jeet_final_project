/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import scriptservice.SOAP;
import scriptservice.SOAP_Service;
import scriptservice.Script;

/**
 *
 * @author student
 */
@Stateless
@LocalBean
public class ControllerBean {

    private SOAP_Service service;

    public List<Script> getAllScripts() {
        checkService();
        SOAP port = service.getSOAPPort();
        return port.getAllScripts();
    }

    public Script getScript(Integer id) {
        checkService();
        SOAP port = service.getSOAPPort();
        return port.getOneScript(id);
    }
    
    public void deleteScript(Integer id) {
        checkService();
        SOAP port = service.getSOAPPort();
        port.deleteScript(id);
    }

    public void uploadScript(String name, String description, String script) {
        checkService();
        SOAP port = service.getSOAPPort();
        port.uploadScript(name, description, script);
    }
    
    
    private void checkService() {
        if(service == null){
            service = new SOAP_Service();
        }
    }
}
